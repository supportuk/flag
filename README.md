## Vložte script

Vložte kód kamkoli do vašich stránek:

	<script src="https://bitbucket.org/supportuk/flag/raw/master/ukflag.js"></script>

## Upravte odsazení shora (volitelné)
Aby vlajka nezasahovala do vaší navigace, můžete vložit do těla stránky element s upravenou css vlastností **top**. např.:

	<div id="uk-flag" style="left:100px;"></div>

Jiného umístění docílíte vlatními css, ale není garantováno správné fungování.
