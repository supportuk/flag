/*!
 * do What The Fuck you want to Public License
 * 
 * Version 1.0, March 2022
 * Copyright (C) 2022 Jan Vrátný.
 * jan@johanies.cz
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 * 
 * Ok, the purpose of this license is simple
 * and you just
 * 
 * DO WHAT THE FUCK YOU WANT TO.
 */

jQueryCode = function(){
  
  var params = {
                  id: "uk-flag",
                  css: {
                      "right": "20px",
                      "top": "20px"
                  }
              };
  var div = $("<div>", params);
  
  var $flag = $('#uk-flag').length ? $('#uk-flag') : div.appendTo('body').addClass('uk-flag');
  
  if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
    $flag.load('http://localhost:8080/flag.html', function(){
      console.log('Flag is ready…');
    })
  }else{
    $flag.html('<style>#uk-flag{ position:fixed; width:100px; z-index:10000001; cursor: pointer;}#uk-flag svg{animation:heartbeat 3s infinite;}#uk-flag:hover #uk-list-of-links{ display: block; transform: translateX(0%); opacity: 1;}@keyframes heartbeat{0%{transform:scale( .95 );}10%{transform:scale( 1 );}20%{transform:scale( .95 );}30%{transform:scale( 1 );}40%{transform:scale( .95 );}100%{transform:scale( .95 );}}/*#fee30c*/#uk-list-of-links{ position: absolute; top: 0%; right: 0; padding: 10px 0; background: #fff; border-radius: 5px; background: #0391ce; transition: all .3s ease; transform: translateX(150%); opacity: 0; font-family: sans-serif; font-size: 16px;/* display: none;*/}#uk-list-of-links a{ display: block; padding: 5px 10px; text-decoration: none; white-space: nowrap; color: #fee30c; transition: all .15s ease;}#uk-list-of-links a:hover{ transform: translateX(3px);}#uk-list-of-links hr{ border: none; border-top: 1px solid #fee30c;}</style><svg width="100%" height="100%" viewBox="0 0 176 176" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><circle cx="87.725" cy="87.725" r="87.725" style="fill:#fff;"/><path d="M17.756,89.333c2.275,6.376 6.062,11.899 10.465,17.032c7.304,8.508 15.839,15.698 24.515,22.714c11.463,9.269 23.024,18.418 34.545,27.619c0.214,0.173 0.438,0.335 0.719,0.544c3.786,-3.042 7.545,-6.045 11.284,-9.074c11.422,-9.243 22.926,-18.389 34.219,-27.787c7.314,-6.088 14.037,-12.812 19.387,-20.77c2.195,-3.262 4.014,-6.674 5.337,-10.278l-140.471,-0Z" style="fill:#ffe30f;fill-rule:nonzero;"/><path d="M160.369,80.134c0.024,-0.226 0.148,-0.443 0.23,-0.664l-0,-7.659c-0.48,-2.344 -0.776,-4.744 -1.465,-7.025c-4.541,-15.062 -14.509,-24.423 -30.005,-27.235c-15.855,-2.878 -28.35,3.107 -37.928,15.729c-1.128,1.487 -2.115,3.085 -3.161,4.619c-1.753,-2.384 -3.355,-4.83 -5.224,-7.048c-5.901,-7 -13.161,-11.84 -22.331,-13.404c-19.019,-3.248 -37.972,7.265 -43.609,27.054c-0.678,2.386 -1.016,4.873 -1.507,7.311l-0,7.659c0.59,2.709 1.002,5.474 1.809,8.116c0.178,0.59 0.373,1.172 0.578,1.747l140.47,-0c1.069,-2.932 1.808,-5.988 2.143,-9.2Z" style="fill:#0791ce;fill-rule:nonzero;"/></svg><div id="uk-list-of-links"> <a target="blank" href="https://www.clovekvtisni.cz/"><b>Člověk v tísni</b> | Peněžité dary &rarr;</a> <hr /> <a target="blank" href="https://www.nasiukrajinci.cz/"><b>Naši Ukrajinci</b> | Webový rozcestník pomoci &rarr;</a> <hr /> <a target="blank" href="https://www.mpsv.cz/web/cz/pomoc-ukrajine"><b>MPSV</b> | Pomoc občanům Ukrajiny &rarr;</a></div>')
  }

  
}

document.addEventListener("DOMContentLoaded", function(event) { 
  if(window.jQuery)  jQueryCode();
  else{   
    var script = document.createElement('script'); 
    document.head.appendChild(script);  
    script.type = 'text/javascript';
    script.src = "//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js";
    script.onload = jQueryCode;
  }
})